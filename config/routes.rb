Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'home#index'

  resources :contacts,                only: [:index, :new, :create, :show]
  resources :reservations,            only: [:index, :new, :create, :show]
 
  namespace :admin do 
    root to: "reservations#index"
    resources :reservations,          only: [:index, :new, :create, :edit, :update, :destroy] do 
      member do 
        patch :cancel
      end
    end
    resources :room_types,            only: [:index, :new, :create, :edit, :update, :destroy]
    resources :room_images,           only: [:index, :new, :create, :edit, :update, :destroy]
    resources :rooms,                 only: [:index, :new, :create, :edit, :update, :destroy]
    resources :accounts,              only: [:index, :new, :create, :edit, :update, :destroy]
    resources :calendar,              only: [:index]
    resources :contacts,              only: [:index, :edit, :update, :destroy]
    resources :charts,                only: [:index]
  end

  namespace :api do 
    resources :charts, only: [:index] do
      collection do
        get :top_books
        get :sales
      end
    end
  end
end
 