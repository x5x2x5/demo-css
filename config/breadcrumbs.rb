crumb :reservations do
  link "Reservations", admin_reservations_path
end

crumb :reservation_create do
  link "Create", new_admin_reservation_path
  parent :reservations
end

crumb :reservation_edit do
  link "Edit", edit_admin_reservation_path
  parent :reservations
end

crumb :room_types do
  link "Room Types", admin_room_types_path
end

crumb :room_type_create do
  link "Create", new_admin_room_type_path
  parent :room_types
end

crumb :room_type_edit do
  link "Edit", edit_admin_room_type_path
  parent :room_types
end

crumb :room_images do
  link "Room Images", admin_room_images_path
end

crumb :room_image_create do
  link "Create", new_admin_room_image_path
  parent :room_images
end

crumb :rooms do
  link "Rooms", admin_rooms_path
end

crumb :room_create do
  link "Create", new_admin_room_path
  parent :rooms
end

crumb :calendar do
  link "Room Calendar", admin_calendar_index_path
end

crumb :contacts do
  link "Contacts", admin_contacts_path
end

# crumb :project do |project|
#   link project.name, project_path(project)
#   parent :projects
# end

# crumb :project_issues do |project|
#   link "Issues", project_issues_path(project)
#   parent :project, project
# end

# crumb :issue do |issue|
#   link issue.title, issue_path(issue)
#   parent :project_issues, issue.project
# end

# If you want to split your breadcrumbs configuration over multiple files, you
# can create a folder named `config/breadcrumbs` and put your configuration
# files there. All *.rb files (e.g. `frontend.rb` or `products.rb`) in that
# folder are loaded and reloaded automatically when you change them, just like
# this file (`config/breadcrumbs.rb`).