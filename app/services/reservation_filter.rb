class ReservationFilter
  include ActiveModel::Model
  attr_accessor :keyword, :status

  def initialize(reservations, attributes = {})
    @reservations = reservations
    attributes.each { |name, value| send("#{name}=", value) }  end

  def result
    @reservations = @reservations.where("reservations.contact_number ILIKE :keyword", keyword: "%#{keyword}%") if keyword.present?
    @reservations = @reservations.where("reservations.status ILIKE :status", status: "%#{status}%") if status.present?

    @reservations
  end

  def persisted?
    false
  end
end