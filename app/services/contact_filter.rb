class ContactFilter
  include ActiveModel::Model
  attr_accessor :arrival_date 
  attr_accessor :departure_date
  attr_accessor :no_of_rooms
  attr_accessor :no_of_persons
end  