class RoomType <  ApplicationRecord
  extend Enumerize

  has_many :room_images
  has_many :reservations
  has_many :rooms

  serialize :amenities, Array
  enumerize :amenities, in: [:fan, :air_conditioner], multiple: true

  validates :name, presence: true

  def price_per_night
    "#{price_per_night_cents} #{price_per_night_currency}"
  end

  def price_per_month
    "#{price_per_month_cents} #{price_per_month_currency}"
  end

  def self.available_types(from, to)
    Reservation.where(arrival_on)
    all.each do |room_type|
    end
  end
end