class Room <  ApplicationRecord
  has_and_belongs_to_many :reservations
  belongs_to :room_type

  validates :name, presence: true
end