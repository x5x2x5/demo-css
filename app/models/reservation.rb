class Reservation <  ApplicationRecord
  extend Enumerize

  belongs_to :room_type
  has_and_belongs_to_many :rooms, optional: true

  validates :name,            presence: true
  validates :contact_number,  presence: true
  validates :email,           presence: true, email: true
  validates :arrival_on,      presence: true
  validates :departure_on,    presence: true
  validates :no_of_persons,   presence: true
  validates :no_of_persons,   numericality: { greater_than: 0 }
  validates :no_of_rooms,     presence: true
  validates :no_of_rooms,     numericality: { greater_than: 0 }
  validates :status,          presence: true
  serialize :status, Array
  enumerize :status, in: [:booking, :expected, :cancelled], default: :expected
  validate :departure_date_is_after_arrival_date
  validate :room_availability

  def price_in(start_date, end_date)
    from = arrival_on
    to = departure_on
    if from < start_date
      from = start_date
    end

    if to > end_date
      to = end_date
    end

    return 0 if from > to
    return (to.to_date - from.to_date).to_i * room_type.price_per_night_cents
  end

  def price_reservation
    price_reservation = (departure_on - arrival_on).to_i * room_type.price_per_night_cents
    price_reservation
  end

  private

  def departure_date_is_after_arrival_date
    return if departure_on.blank? || arrival_on.blank?

    if departure_on < arrival_on
      errors.add(:departure_on, "cannot be before the arrival date") 
    end 
  end

  def room_availability
    rooms.each do |room|
      available = true

      room.reservations.each do |r|
        if (
          (r.id != id) &&
          !(departure_on <= r.arrival_on || r.departure_on <= arrival_on)
        )
          available = false
        end
      end

      errors.add(:room_ids, "#{room.name} is not available") if !available
    end
  end
end