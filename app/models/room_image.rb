class RoomImage <  ApplicationRecord
  belongs_to :room_type

  has_attached_file :image, styles: { medium: "850x900>", thumb: "400x320>" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
end