DemoCssFisrtProject.admin_calendar = {
  index: {
    init: function() {
      const data = JSON.parse($('#calendar').attr('data'));

      const events = [];

      $.each(data, function(index, reservation) { 
        events.push({
          id: reservation.id,
          title: `${reservation.rooms} - ${reservation.name} - ${reservation.contact_number}`,
          start: new Date(reservation.arrival_date),
          end: new Date(reservation.departure_date)
        })
      })

      $('#calendar').fullCalendar({
        displayEventTime: false,
        header: {
          left: 'prev,next',
          center: 'title',
          right: 'today'
        },
        eventSources: [{
          events: events,
          color: '#f69679', // an option!
          textColor: 'black', // an option!,
        }],
        eventLimit: true, // for all non-agenda views
        eventLimitText: "Something",
        eventAfterRender: function(event, element, view) {
          // $(element).css('line-height', '27px');
          // $(element).css('font-family', '"Bai Jamjuree", sans-serif');
          // $(element).css('padding', '2px');
          // $(element).css('cursor', 'pointer');
          // $(element).css('font-size', '18px');
          // $(element).parents('.fc-popover').first().css('width', '400px');
        },
        views: {
          month: {
            eventLimit: 4,
            eventLimitText: "More"
          }
        },
        eventClick: function(calEvent, jsEvent, view) {
          window.location = `/admin/reservations/${calEvent.id}/edit`;
        }
      });
    }
  }
}