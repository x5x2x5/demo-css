DemoCssFisrtProject.shared = {
  menu: {
    init: function() {
      $("#sidebarBtn").click(function() {
        if($('body').hasClass("sidebar-collapsed")) {
          $('body').removeClass("sidebar-collapsed");
          $('body').addClass("sidebar-uncollapsed");
        }
        else if($('body').hasClass("sidebar-uncollapsed")) {
          $('body').removeClass("sidebar-uncollapsed");
          $('body').addClass("sidebar-collapsed");
        } else {
          $('body').addClass("sidebar-collapsed");
        }
      });
    }
  }
}