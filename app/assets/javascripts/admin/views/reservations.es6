DemoCssFisrtProject.admin_reservations = {
  new: {
    init: function() {
      const reservationArrivalOn = $("#reservation_arrival_on")
      const reservationDepartureOn = $("#reservation_departure_on")
      $("#reservation_arrival_on").datepicker('setStartDate', new Date());
      $("#reservation_arrival_on").datepicker().on('changeDate', function(e) {
        reservationDepartureOn.datepicker('setStartDate', moment(reservationArrivalOn.val()).add(1, 'day').toDate());
      });
      $("#reservation_departure_on").datepicker().on('changeDate', function(e) {
        $("#reservation_arrival_on").datepicker('setEndDate', new Date(reservationArrivalOn.val()));
        reservationArrivalOn.datepicker('setEndDate', moment(reservationDepartureOn.val()).add(-1, 'day').toDate());
      });
      if(new Date(reservationArrivalOn.val()) >= new Date(reservationDepartureOn.val())) {
        reservationDepartureOn.val("")
      }
    }
  }
}