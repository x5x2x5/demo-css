DemoCssFisrtProject.admin_charts = {
  index: {
    init: function() {
      fetch("//localhost:3001/api/charts/top_books")
        .then(res => res.json())
        .then(top_books => {
          var chart = new CanvasJS.Chart("chartContainer", {
            animationEnabled: true,
            title: {
              text: "Top Booking - 2019"
            },
            data: [{
              type: "pie",
              startAngle: 240,
              yValueFormatString: "##0.00\"%\"",
              indexLabel: "{label} {y}",
              dataPoints: top_books.map(({name, percent}) => ({
                y:percent, label: name
              }))
            }]
          });
          chart.render();
        }) 
    }
  }
}