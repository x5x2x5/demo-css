const Utils = {
  init: () => {
    Utils.initDatePicker()
    Utils.initSelectPicker()
  },

  initSelectPicker: () => {
    const selects = $("form.simple_form select").not('.jselect').addClass('jselect')

    selects.each(function(index, select) {
      $(select).selectpicker();
    })
  },

  initDatePicker:() => {
    $('input.date_picker:not(.jdate-picker)')
      .addClass('jdate-picker')
      .datepicker({
        format: 'yyyy-mm-dd'
      }); 
  },
}
