const getCurrentStep = () => {
  return $('ul.steps').find('.step.active').index() + 1
}

const activeStep = (step) => {
  let index = step - 1

  index = index < 0 ? 0 : index;
  index = index > 3 ? 3 : index;

  const tabs = $('ul.steps').find('.step')
  const contents = $('div.steps').find('.step')

  tabs.removeClass('active').removeClass('completed')
  contents.removeClass('active').removeClass('completed')

  tabs.each((i, tab) => {
    if(i < index) {
      tabs.eq(i).addClass('completed')
      contents.eq(i).addClass('completed')
    }
    else if(i === index) {
      tabs.eq(i).addClass('active')
      contents.eq(i).addClass('active')
    }
  })
}

const nextStep = () => {
  activeStep(getCurrentStep() + 1)
}

const prevStep = () => {
  activeStep(getCurrentStep() - 1)
}


DemoCssFisrtProject.reservations = {
  new: {
    init: function() {
      activeStep(1);
      $(".btn-next").click(function() {
        const currentStep = $(this).parents('.step').first();
        let isValid = true;

        $('input.required').removeClass('error');

        currentStep.find('input.required').each(function() {
          if ($(this).val() === "") {
            isValid = false;
            $(this).addClass('error')
          }
        });

        $("#reservation_no_of_persons, #reservation_no_of_rooms").each(function() {
          if ($(this).val() < 1) {
            isValid = false;
            $(this).addClass('error')
          } 
        })

        if(isValid) {
          nextStep();
        }

        return false;
      })

      $(".btn-confirm").click(function() {
        const currentStep = $(this).parents('.step').first();
        let isValid = true;
        const testMail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
        const reservationMail = $('input#reservation_email');

        $('input.required').removeClass('error');
        currentStep.find('input.required').each(function() {
          if ($(this).val() === "") {
            isValid = false;
            $(this).addClass('error')
          }
        });

        if(isValid) {
          return true
        }

        return false;
      })

      $(".take-room").click(function() {
        const roomId = $(this).data("room-type-id")
        $("#reservation_room_type_id").val(roomId)
      })
    }
  }
}