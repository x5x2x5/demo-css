const getCurrent = () => {
  return $('.rooms').find('section.home-room.active').index();
}

const active = (step) => {
  const tabs = $('div.rooms').find('section.home-room');

  tabs.removeClass('active')

  tabs.each((i, tab) => {
    if(i === step) {
      tabs.eq(i).addClass('active')
    }
  })
}

const next = () => {
  let nextStep = getCurrent() + 1;
  const lastStep = $('section.home-room').last().index();
  nextStep = nextStep > lastStep ? 0 : nextStep;
  active(nextStep);
}

const prev = () => {
  let preStep = getCurrent() - 1;
  const lastStep = $('section.home-room').last().index();
  preStep = preStep >= 0 ? preStep : lastStep;
  active(preStep)
}

DemoCssFisrtProject.home = {
  index: {
    init: function() {
      next(1);
      $(".next_home").click(() => {
        next();
        return false;
      })
      $(".prev_home").click(() => {
        prev();
        return false;
      })
    }
  }
}