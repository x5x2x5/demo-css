//= require jquery
//= require jquery-ui
//= require rails-ujs
//= require activestorage
//= require moment 
//= require fullcalendar
//= require fullcalendar/locale-all
//= require chartkick
//= require Chart.bundle

//= require turbolinks
//= require bootstrap-sprockets
//= require datepicker/js/bootstrap-datepicker
//= require bootstrap_select/js/bootstrap-select
//= require jquery.canvasjs.min
//= require moment

//= require admin/utils
//= require admin/global

//= require admin/views/shared
//= require admin/views/calendar/index
//= require admin/views/reservations
//= require admin/views/charts


