//= require rails-ujs
//= require activestorage
//= require turbolinks
//= require jquery
//= require bootstrap-sprockets
//= require datepicker/js/bootstrap-datepicker
//= require flexslider/flexslider
//= require moment

//= require utils
//= require global

//= require pages/reservations
//= require pages/home