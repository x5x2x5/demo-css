class HomeController < ApplicationController
  def index
    @reservation = Reservation.new
    @room_types = RoomType.all.includes(:room_images)
    @room_types_amount = RoomType.all.length
  end
end
