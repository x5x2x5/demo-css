class ContactsController < ApplicationController
  def new
    @contact = Contact.new
    @title = "Contact Us"
  end

  def create
    @title = "Contact Us"
    @contact = Contact.new contact_params

    if @contact.save
      redirect_to contact_path(@contact), notice: "Save successfully"
    else
      render 'new', notice: "Please repeat"
    end
  end

  def show
    @contact = Contact.find(params[:id])
    @title = "Contact Us"
  end

  private

  def contact_params
    params.require(:contact).permit(
      :name,
      :email,
      :contact_number,
      :subject
    )
  end
end
