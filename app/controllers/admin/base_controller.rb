class Admin::BaseController < ActionController::Base
  layout 'admin/layouts/main'
  before_action :authenticate_user!
end