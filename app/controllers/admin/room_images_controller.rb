class Admin::RoomImagesController < Admin::BaseController
  def index
    @room_images = RoomImage.all.paginate(:page => params[:page], :per_page => 4)
  end

  def new
    @room_image = RoomImage.new
  end

  def create
    @room_image = RoomImage.new room_image_params

    if @room_image.save
      redirect_to admin_room_images_path, notice: "Save successfully"
    else
      render 'new', notice: "Please repeat"
    end  
  end

  def edit
    @room_image = RoomImage.find(params[:id])
  end

  def update
    @room_image = RoomImage.find(params[:id])  
    if @room_image.update room_image_params
      redirect_to admin_room_images_path, notice: "Update successfully"
    else
      render 'edit', notice: "Update error"
    end
  end

  def destroy
    @room_image = RoomImage.find(params[:id])
    @room_image.destroy
    redirect_to admin_room_images_path
  end

  private

  def room_image_params
    params.require(:room_image).permit(:image, :room_type_id)
  end
end