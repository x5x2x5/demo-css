class Admin::ContactsController < Admin::BaseController
  def index
    @contacts = Contact.all.paginate(:page => params[:page], :per_page => 4)
  end

  def destroy
    @contact = Contact.find(params[:id])
    @contact.destroy
    redirect_to admin_contacts_path
  end
end
