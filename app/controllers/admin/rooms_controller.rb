class Admin::RoomsController < Admin::BaseController
  def index
    @rooms = Room.all.paginate(:page => params[:page], :per_page => 4)
  end

  def new
    @room = Room.new
  end

  def create
    @room = Room.new room_params

    if @room.save
      redirect_to admin_rooms_path, notice: "Save successfully"
    else
      render 'new', notice: "Please repeat"
    end  
  end

  def edit
    @room = Room.find(params[:id])
  end

  def update
    @room = Room.find(params[:id])  
    if @room.update room_params
      redirect_to admin_rooms_path, notice: "Update successfully"
    else
      render 'edit', notice: "Update error"
    end
  end

  def destroy
    @room = Room.find(params[:id])
    @room.destroy
    redirect_to admin_rooms_path
  end

  private

  def room_params
    params.require(:room).permit(:name, :room_type_id)
  end
end