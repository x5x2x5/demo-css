class Admin::CalendarController < Admin::BaseController
  def index
    @data = []
    Reservation.all.includes(:rooms).each do |reservation|
      @data << {
        id: reservation.id,
        name: reservation.name,
        rooms: reservation.rooms.map(&:name).join(" - "),
        contact_number: reservation.contact_number,
        arrival_date: reservation.arrival_on,
        departure_date: reservation.departure_on.end_of_day
      }
    end
  end
end