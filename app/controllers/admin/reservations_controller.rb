class Admin::ReservationsController < Admin::BaseController
  load_and_authorize_resource
  
  def index
    @reservations = Reservation.all
    @reservation_filter = ReservationFilter.new(@reservations, reservation_filter_params)
    @reservations = @reservation_filter.result.paginate(:page => params[:page], :per_page => 4)
  end
  def new
    @reservation = Reservation.new
  end
  def create
    @reservation = Reservation.new reservation_params

    if @reservation.save
      redirect_to admin_reservations_path, notice: "Save successfully"
    else
      render 'new', notice: "Please repeat"
    end  
  end

  def edit
    @reservation = Reservation.find(params[:id])
  end

  def update
    @reservation = Reservation.find(params[:id])  
    if @reservation.update reservation_params
      redirect_to admin_reservations_path, notice: "Update successfully"
    else
      render 'edit', notice: "Update error"
    end
  end

  def cancel
    @reservation = Reservation.find(params[:id])
    @reservation.status = :cancelled
    if @reservation.save
      redirect_to admin_reservations_path, notice: "Cancel successfully"
    else
      render 'edit', notice: "Cancel error"
    end  
  end

  private

  def reservation_params
    params.require(:reservation).permit(
      :name, 
      :contact_number, 
      :email, 
      :nric, 
      :arrival_on, 
      :departure_on, 
      :no_of_persons, 
      :no_of_rooms, 
      :remark, 
      :room_type_id,
      :status,
      room_ids: []
    )
  end

  def reservation_filter_params
    return {} if params[:reservation_filter].nil?

    params.require(:reservation_filter).permit(
      :keyword,
      :status
    )
  end
end