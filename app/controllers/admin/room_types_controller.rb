class Admin::RoomTypesController < Admin::BaseController
  def index
    @room_types = RoomType.all.paginate(:page => params[:page], :per_page => 4)
  end
  def new
    @room_type = RoomType.new
  end
  def create
    @room_type = RoomType.new room_type_params

    if @room_type.save
      flash[:notice] = "Save successfully"
      redirect_to admin_room_types_path
    else
      flash[:error] = "Please repeat"
      render 'new'
    end  
  end

  def edit
    @room_type = RoomType.find(params[:id])
  end

  def update
    @room_type = RoomType.find(params[:id])  
    if @room_type.update room_type_params
      redirect_to admin_room_types_path, notice: "Update successfully"
    else
      render 'edit', notice: "Update error"
    end
  end

  def destroy
    @room_type = RoomType.find(params[:id])
    @room_type.destroy
    redirect_to admin_room_types_path
  end

  private

  def room_type_params
    params.require(:room_type).permit(:name, :capacity, :price_per_night_cents, :price_per_month_cents, :description, :size, amenities: [])
  end
end