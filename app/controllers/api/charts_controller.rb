class Api::ChartsController < Api::BaseController
  include DateTimeHelper

  def sales
    @reservations = Reservation.all
    res = {}
    from = Date.today.at_beginning_of_year
    to = Date.today.at_end_of_year
    start_date = from
    
    while start_date <= to  do
      end_date = start_date.at_end_of_month
      total = 0
      @reservations.each do |r|
        if r.status == "expected" 
          total += r.price_in(start_date, end_date)
        end
      end
      res[format_date(start_date)] = total
      start_date = start_date.next_month
    end

    render json: res
  end

  def top_books
    count_reservations = Reservation.all.count
    room_types = RoomType.all.includes(:reservations)
    res = []

    room_types.each do |r|
      percent = r.reservations.ids.count.to_f/count_reservations * 100
      res << {
        name: r.name,
        percent: percent.round
      }
    end

    render json: res
  end
end