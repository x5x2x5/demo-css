class ReservationsController < ApplicationController
  def new
    @reservations = Reservation.all
    @reservation = Reservation.new(reservation_params)
    @title = "Make Reservation"
    @room_types = RoomType.all
  end

  def create
    @room_types = RoomType.all
    @reservation = Reservation.new reservation_params
    @title = "Make Reservation"
    
    if @reservation.save
      redirect_to reservation_path(@reservation), notice: "Save successfully"
    else
      render 'new', notice: "Please repeat"
    end  
  end

  def show
    @reservation = Reservation.find(params[:id])
    @title = "Make Reservation"
  end

  private

  def reservation_params
    params.require(:reservation).permit(
      :name, 
      :contact_number, 
      :email, 
      :nric, 
      :arrival_on, 
      :departure_on, 
      :no_of_persons, 
      :no_of_rooms, 
      :remark, 
      :room_type_id, 
      room_ids: []
    )
  end
end
