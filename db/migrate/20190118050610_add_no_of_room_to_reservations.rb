class AddNoOfRoomToReservations < ActiveRecord::Migration[5.2]
  def change
    add_column :reservations, :no_of_rooms, :integer
  end
end
