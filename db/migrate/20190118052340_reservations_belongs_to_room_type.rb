class ReservationsBelongsToRoomType < ActiveRecord::Migration[5.2]
  def change
    add_reference :reservations, :room_type, foreign_key: true 
  end
end
