class AddNoOfChildrenToReservations < ActiveRecord::Migration[5.2]
  def change
    add_column :reservations, :no_of_children, :integer
  end
end
