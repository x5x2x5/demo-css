class CreateRoomImages < ActiveRecord::Migration[5.2]
  def change
    create_table :room_images do |t|
      t.belongs_to :room_type, index: true
      t.timestamps
    end
  end
end
