class CreateContacts < ActiveRecord::Migration[5.2]
  def change
    create_table :contacts do |t|
      t.string :name
      t.string :email
      t.string :contact_number
      t.text :subject
      t.timestamps
    end
  end
end
