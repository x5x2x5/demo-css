class RemoveRoomTypeIdToRooms < ActiveRecord::Migration[5.2]
  def change
    remove_column :rooms, :room_type_id
  end
end
