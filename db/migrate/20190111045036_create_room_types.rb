class CreateRoomTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :room_types do |t|
      t.string    :name
      t.integer   :capacity
      t.monetize  :price_per_night
      t.monetize  :price_per_month
      t.text      :description
      t.string    :amenities
      t.float     :size
      t.timestamps
    end
  end
end
