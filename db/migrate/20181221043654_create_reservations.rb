class CreateReservations < ActiveRecord::Migration[5.2]
  def change
    create_table :reservations do |t|
      t.string :contact_number
      t.string :email
      t.string :nric
      t.date :arrival_on
      t.date :departure_on
      t.integer :no_of_persons
      t.text :remark
      t.timestamps
    end
  end
end
