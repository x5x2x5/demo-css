class CreateJoinTableReservationRoom < ActiveRecord::Migration[5.2]
  def change
    create_join_table :reservations, :rooms do |t|
      t.index [:reservation_id, :room_id]
    end
  end
end
