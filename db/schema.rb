# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_07_15_011827) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "contacts", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "contact_number"
    t.text "subject"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "reservations", force: :cascade do |t|
    t.string "contact_number"
    t.string "email"
    t.string "nric"
    t.date "arrival_on"
    t.date "departure_on"
    t.integer "no_of_persons"
    t.text "remark"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.integer "no_of_rooms"
    t.bigint "room_type_id"
    t.string "status"
    t.integer "no_of_children"
    t.index ["room_type_id"], name: "index_reservations_on_room_type_id"
  end

  create_table "reservations_rooms", id: false, force: :cascade do |t|
    t.bigint "reservation_id", null: false
    t.bigint "room_id", null: false
    t.index ["reservation_id", "room_id"], name: "index_reservations_rooms_on_reservation_id_and_room_id"
  end

  create_table "room_images", force: :cascade do |t|
    t.bigint "room_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_file_name"
    t.string "image_content_type"
    t.integer "image_file_size"
    t.datetime "image_updated_at"
    t.index ["room_type_id"], name: "index_room_images_on_room_type_id"
  end

  create_table "room_types", force: :cascade do |t|
    t.string "name"
    t.integer "capacity"
    t.integer "price_per_night_cents", default: 0, null: false
    t.string "price_per_night_currency", default: "USD", null: false
    t.integer "price_per_month_cents", default: 0, null: false
    t.string "price_per_month_currency", default: "USD", null: false
    t.text "description"
    t.string "amenities"
    t.float "size"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rooms", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "room_type_id"
    t.index ["room_type_id"], name: "index_rooms_on_room_type_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "type"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "reservations", "room_types"
  add_foreign_key "rooms", "room_types"
end
